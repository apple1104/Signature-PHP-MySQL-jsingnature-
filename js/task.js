
$(document).ready(function () {

    $("input").focusout(function () {
        var inputvalue = $(this).val();
        if (inputvalue == '') {
            var thisId = $(this).attr('id');
            var error = "Please enter " + thisId + "."
            $(this).attr('placeholder', error);
        }

    });

    $("#Presidente").jSignature();
    $("#Richiedente").jSignature();

});
