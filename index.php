<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="./css/inputstyle.css">
    <link rel="stylesheet" href="./css/style.css">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" crossorigin="anonymous"></script>
    <script type="text/javascript" src="./js/jquery-3.4.1.min.js"></script>
    <script src="./js/task.js"></script>
    <script src="./js/function.js"></script>
    <script src="https://cdn.bootcss.com/layer/2.3/layer.js"></script>
    <script type="text/javascript" src="./libs/jSignature.min.js"></script>
    <script type="text/javascript" src=" https://cdn.bootcss.com/layer/2.3/layer.js"></script>
    <title>DONK CLUB</title>
</head>
<?php
$host         = "localhost";
$username     = "root";
$password     = "";
$dbname       = "singnature";
$result = 0;

/* Create connection */
$conn = new mysqli($host, $username, $password, $dbname);
/* Check connection */
if ($conn->connect_error) {
     die("Connection to database failed: " . $conn->connect_error);
}
?>
<body>
    <div class="container" id="content">
        <div class="empty"></div>
        <div class="border title center">
            <h1>DONK CLUB</h1>
            <h2>CIRCOLO AFFICIATO FENALC</h2>
        </div>
        <br>
        <div class="center">
            <h3 id="bg_grey">DOMANDA DI AMMISSIONE A SOCIO</h3>
            <p class="rofa">(Ai sensi dello Statuto Sociale)</p>
        </div>
        <div>
            <table class="center mytable">
                <tbody>
                    <tr>
                        <td>
                            <span>
                                <input id="RINNOVO" type="radio" name="RINNOVO" value="RINNOVO" checked> RENUOVO
                            </span>
                            &nbsp; &nbsp;
                            <span>
                                <input id="NUOVO" type="radio" name="RINNOVO" value="NUOVO"> NUOVO
                            </span>
                        </td>
                        <td id="mypadding">
                            DATA RICHIESTA
                            <span class="bmd-form-group">
                                <input type="date" id="RICHIESTA" class="form-control" name="RICHIESTA" min="2020-01-01"
                                    max="2020-12-31" value="" required="required">
                            </span>
                        </td>
                        <td>TESSERA N.
                            <span class="bmd-form-group">
                                <input type="text" id="TESSERA" class="form-control" name="TESSERA" value=""
                                    required="required">
                            </span>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <br>
        <div>
            <div>
                <span class="bmd-form-group">
                    <div class="input-group-custom">
                        <div class="input-group-prepend">
                            <span class="input-group-text">
                                NOME
                            </span>
                        </div>&nbsp;&nbsp;&nbsp;
                        <input type="text" id="NOME" class="form-control" name="NOME" value="" required="required">
                        &nbsp;&nbsp;&nbsp;
                        <span class="input-group-text">CONGNAME</span>&nbsp;&nbsp;&nbsp;
                        <input type="text" id="CONGNAME" class="form-control" name="CONGNAME" value="" required="required">
                    </div>
                </span>
            </div>
            <div>
                <span class="bmd-form-group">
                    <div class="input-group-custom">
                        <div class="input-group-prepend">
                            <span class="input-group-text"> Nato a</span>
                        </div>&nbsp;&nbsp;&nbsp;
                        <input type="text" id="Nato" class="form-control" name="Nato" value="" required="required">
                        &nbsp;&nbsp;&nbsp;
                        <span class="input-group-text">il</span>&nbsp;&nbsp;&nbsp;
                        <input type="date" id="il" class="form-control" name="il" value="" required="required">
                    </div>
                </span>
            </div>
            <div>
                <span class="bmd-form-group">
                    <div class="input-group-custom">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Residente a
                            </span>
                        </div>&nbsp;&nbsp;&nbsp;
                        <input type="text" id="Residente" class="form-control" name="Residente" value="" required="required">
                        &nbsp;&nbsp;&nbsp;
                        <span class="input-group-text">Cap.</span>&nbsp;&nbsp;&nbsp;
                        <input type="text" id="Cap" class="form-control" name="Cap" value="" required="required">
                    </div>
                </span>
            </div>
            <div>
                <span class="bmd-form-group">
                    <div class="input-group-custom" id="valid1">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Via</span>
                        </div>&nbsp;&nbsp;&nbsp;
                        <input type="number" id="Via" class="form-control" name="Via" value="" required="required">

                        &nbsp;&nbsp;&nbsp;
                        <span class="input-group-text">
                            Cell.
                        </span>&nbsp;&nbsp;&nbsp;
                        <input type="number" id="Cell" class="form-control" name="Cell" value="" required="required">

                    </div>
                </span>
            </div>
            <div>
                <span class="bmd-form-group">
                    <div class="input-group-custom">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Indirizzo e-mail</span>
                        </div>&nbsp;&nbsp;&nbsp;
                        <input type="email" id="e-mail" class="form-control" name="e-mail" value="" required="required">
                    </div>
                </span>
            </div>
        </div>
        <br> <br>
        <div>
            <p>Chiede con la firma della presente di poter essere ammesso in qualità di socio all’associazione “DONK
                CLUB“</p>
            <p>Dichiaro di aver letto e di rispettare lo statuto e il regolamento del sodalizio e di accettarli in ogni
                suo punto.</p>
            <p>Dichiaro di impegnarmi al pagamento della quota associativa e ai contributi associativi a seconda
                dell’attività svolta ed alle modalità prescelte.</p>
            <p>In ogni caso prendo atto che tutte le affissioni con gli eventi, le attività e le assemblee, saranno
                riportate nell’apposito spazio destinato alle comunicazioni.</p>
            <p>Ai sensi e per gli effetti degli articoli 1341 e 1342 del C.C., dichiaro di aver letto e di aver ben
                compreso nonché di approvare espressamente le condizioni e le pattuizioni previste dallo statuto, dal
                regolamento e dal contratto assicurativo derivante dal tesseramento.</p>
            <p>Dichiaro di aver letto l’informativa sulla privacy apposta nell’apposito spazio e di firmare in calce per
                accettazione e consenso al trattamento dei dati personali ai sensi del GDPR Regolamento (UE) 2016/679
            </p>
        </div>
        <br><br>
        <div>
            <table>
                <tr id="left">
                    <td>
                        <span> Firma del Richiedente</span>
                        <i class="bmd-form-group">
                            <div id="Richiedente"></div> <button class="btn btn-info" id="uclear">Clear</button>
                        </i>
                    </td>
                    <td>
                        <span> Firma del Presidente</span>
                        <span class="bmd-form-group">
                            <div id="Presidente"></div><button class="btn btn-info" id="rclear">Clear</button>
                        </span>
                    </td>
                </tr>
            </table>
        </div>
        <br>
        <div class="center">
            <button class="btn btn-success" id="toExcel" data-toggle="modal" data-target="#exampleModal">O K</button>
        </div>
    </div>
    <div class="empty"></div>
    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                <table class="table table-bordered" id="tblData">
                    <thead>
                        <th>No</th>
                        <th>NUOVO</th>
                        <th>RICHIESTA</th>
                        <th>Tessera</th>
                        <th>Nome</th>
                        <th>Congname</th>
                        <th>Nato</th>
                        <th>il</th>
                        <th>Residente</th>
                        <th>Cap</th>
                        <th>Via</th>
                        <th>Cell</th>
                        <th>e-mail</th>
                        <th>Print</th>
                    </thead>
                    <tbody>
                        <?php
                            $sql = "SELECT * FROM user";
                            $result = $conn->query($sql);
                            $i=0;
                                while($row = $result->fetch_assoc()) {
                                    $i++;
                                    echo "<tr>";
                                    echo "<td>" . $i . "</td>";
                                    echo "<td>" . $row['user_info'] . "</td>";
                                    echo "<td>" . $row['register_date'] . "</td>";
                                    echo "<td>" . $row['card_number'] . "</td>";
                                    echo "<td>" . $row['fname'] . "</td>";
                                    echo "<td>" . $row['surname'] . "</td>";
                                    echo "<td>" . $row['birth_place'] . "</td>";
                                    echo "<td>" . $row['birthday'] . "</td>";
                                    echo "<td>" . $row['live_city'] . "</td>";
                                    echo "<td>" . $row['cap'] . "</td>";
                                    echo "<td>" . $row['via'] . "</td>";
                                    echo "<td>" . $row['cell'] . "</td>";
                                    echo "<td>" . $row['email'] . "</td>";
                                    echo "<td><a href='php/print.php?id=".$row['id']."'>Create Pdf</a></td>";
                                    echo "</tr>";
                        ?>
                        <?php
                                }
                        ?>
                    </tbody>
                </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div class="empty"></div>                   
</body>
<script>
    $(document).ready(function () {
    $("#uclear").on('click', function(){ 
        $("#Richiedente").jSignature("clear");Richiedente
    });
    $("#rclear").on('click', function(){ 
        $("#Presidente").jSignature("clear");
    });

});
</script>
</html>